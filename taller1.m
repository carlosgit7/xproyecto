
x=-10:10;
y=x.^2;
plot(x,y);
hold

p=-9;
alpha=0.8;

fp=inline('2*x');
f=inline('x^2');

while ( f(p) >= 10^-8)
    p=p-(alpha*fp(p));
    plot(p,0,'o');
    hold on 
end 
